package com.android.gl2jni;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.libsdl.app.SDLActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;


public class GL2JNIActivity extends SDLActivity {

    GL2JNIView mView;
    TextView tv;
    Button button;
    /* A fancy way of getting the class name */
    private static final String TAG = GL2JNIActivity.class.getSimpleName();

    /* A list of assets to copy to internal directory */
    private static final String[] ASSET_NAMES = new String[]{"bump.jpg",
            "skybox-negx.jpg",
            "skybox-negy.jpg",
            "skybox-negz.jpg",
            "skybox-posx.jpg",
            "skybox-posy.jpg",
            "skybox-posz.jpg",
            "Roboto-Medium.ttf"
    };

    @Override
    protected String[] getLibraries() {
        return new String[]{"hidapi", "SDL2", "demo"};
    }

    @Override
    protected String[] getArguments() {
        return new String[]{getFilesDir().getAbsolutePath()};
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        /* We're going to unpack our assets to a location that's accessible
         * via the usual stdio means. This is to avoid using AAssetManager
         * in our native code. */
        Log.v(TAG, "Copying assets to accessible locations");
        AssetManager assetManager = this.getAssets();
        for (String assetName: ASSET_NAMES) {
            try {
                Log.v(TAG, "Copying " + assetName);
                InputStream ais = assetManager.open(assetName);
                FileOutputStream fos = openFileOutput(assetName, MODE_PRIVATE);
                final int BUFSZ = 8192;
                byte[] buffer = new byte[BUFSZ];
                int readlen = 0;
                do {
                    readlen = ais.read(buffer, 0, BUFSZ);
                    if (readlen < 0) {
                        break;
                    }
                    fos.write(buffer, 0, readlen);
                } while (readlen > 0);
                fos.close();
                ais.close();
            } catch(IOException e){
                Log.e(TAG, "Could not open " + assetName + " from assets, that should not happen", e);
            }
        }

        mView = new GL2JNIView(getApplication());
	    setContentView(mView);
	    //add a button
        addListenerOnButton();
        // creating LinearLayout
        LinearLayout linLayout = new LinearLayout(this);
        // specifying vertical orientation
        linLayout.setOrientation(LinearLayout.VERTICAL);
        // creating LayoutParams
        ViewGroup.LayoutParams linLayoutParam = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        // set LinearLayout as a root element of the screen

        //display button
        linLayout.addView(button, linLayoutParam);

        tv = new TextView(this);
        tv.setText("TextView");
        tv.setLayoutParams(linLayoutParam);
        linLayout.addView(tv);

        //Button btn = new Button(this);
        //btn.setText("Button");


        addContentView(linLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    @Override protected void onPause() {
        super.onPause();
        mView.onPause();
    }

    @Override protected void onResume() {
        super.onResume();
        mView.onResume();
    }

    public void addListenerOnButton() {

        button = new Button(this); //(Button) findViewById(R.id.button1);
        //button.setHeight(10);
        //button.setWidth(50);
        button.setText("Press to get string from JNI");
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                tv.setText(stringFromJNI());
                //Intent browserIntent =
                //        new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
                //startActivity(browserIntent);

            }

        });

    }


    public native void startTicks();
    public native void StopTicks();
    public native  String stringFromJNI();
}
