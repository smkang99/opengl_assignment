//
// Created by Seungmo Kang on 2/7/19.
//

#include <jni.h>
#include <android/log.h>

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define  LOG_TAG    "libgl2jni"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

extern "C" {
JNIEXPORT void JNICALL Java_com_android_gl2jni_GL2JNIActivity_startTicks(JNIEnv * env, jobject obj);
JNIEXPORT void JNICALL Java_com_android_gl2jni_GL2JNIActivity_stopTicks(JNIEnv * env, jobject instance);
JNIEXPORT jstring JNICALL Java_com_android_gl2jni_GL2JNIActivity_stringFromJNI(JNIEnv * env, jobject obj);
};

JNIEXPORT void JNICALL
Java_com_android_gl2jni_GL2JNIActivity_startTicks( JNIEnv * env, jobject obj )
{
#if defined(__arm__)
    #if defined(__ARM_ARCH_7A__)
    #if defined(__ARM_NEON__)
      #if defined(__ARM_PCS_VFP)
        #define ABI "armeabi-v7a/NEON (hard-float)"
      #else
        #define ABI "armeabi-v7a/NEON"
      #endif
    #else
      #if defined(__ARM_PCS_VFP)
        #define ABI "armeabi-v7a (hard-float)"
      #else
        #define ABI "armeabi-v7a"
      #endif
    #endif
  #else
   #define ABI "armeabi"
  #endif
#elif defined(__i386__)
#define ABI "x86"
#elif defined(__x86_64__)
#define ABI "x86_64"
#elif defined(__mips64)  /* mips64el-* toolchain defines __mips__ too */
#define ABI "mips64"
#elif defined(__mips__)
#define ABI "mips"
#elif defined(__aarch64__)
#define ABI "arm64-v8a"
#else
#define ABI "unknown"
#endif
    //return env->NewStringUTF("Hello from JNI !  Compiled with ABI " ABI ".");
    //int size = 16;
    //char r[] = {'P', 'K', 'd', 'h', 't', 'X', 'M', 'm', 'r', '1', '8', 'n', '2', 'L', '9', 'K'};
    //jbyteArray array = env->NewByteArray(size);
    //env->SetByteArrayRegion(array, 0, size, r);
    //jstring strEncode = env->NewStringUTF("UTF-8");
    //jclass cls = env->FindClass("java/lang/String");
    //jmethodID ctor = env->GetMethodID(cls, "<init>", "([BLjava/lang/String;)V");
    //jstring object = (jstring) env->NewObject(cls, ctor, array, strEncode);

    //return object;
}

JNIEXPORT void JNICALL
Java_com_android_gl2jni_GL2JNIActivity_StopTicks(JNIEnv *env, jobject instance){

}

JNIEXPORT jstring JNICALL
Java_com_android_gl2jni_GL2JNIActivity_stringFromJNI( JNIEnv * env, jobject obj )
{
#if defined(__arm__)
    #if defined(__ARM_ARCH_7A__)
    #if defined(__ARM_NEON__)
      #if defined(__ARM_PCS_VFP)
        #define ABI "armeabi-v7a/NEON (hard-float)"
      #else
        #define ABI "armeabi-v7a/NEON"
      #endif
    #else
      #if defined(__ARM_PCS_VFP)
        #define ABI "armeabi-v7a (hard-float)"
      #else
        #define ABI "armeabi-v7a"
      #endif
    #endif
  #else
   #define ABI "armeabi"
  #endif
#elif defined(__i386__)
#define ABI "x86"
#elif defined(__x86_64__)
#define ABI "x86_64"
#elif defined(__mips64)  /* mips64el-* toolchain defines __mips__ too */
#define ABI "mips64"
#elif defined(__mips__)
#define ABI "mips"
#elif defined(__aarch64__)
#define ABI "arm64-v8a"
#else
#define ABI "unknown"
#endif
    return env->NewStringUTF("String  returned from JNI !  Compiled with ABI " ABI ".");
    //int size = 16;
    //char r[] = {'P', 'K', 'd', 'h', 't', 'X', 'M', 'm', 'r', '1', '8', 'n', '2', 'L', '9', 'K'};
    //jbyteArray array = env->NewByteArray(size);
    //env->SetByteArrayRegion(array, 0, size, r);
    //jstring strEncode = env->NewStringUTF("UTF-8");
    //jclass cls = env->FindClass("java/lang/String");
    //jmethodID ctor = env->GetMethodID(cls, "<init>", "([BLjava/lang/String;)V");
    //jstring object = (jstring) env->NewObject(cls, ctor, array, strEncode);

    //return object;
}